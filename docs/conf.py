# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os
import sys
sys.path.insert(0, os.path.abspath('..'))


project = 'Project Icaras - Group 11'
copyright = '2024, Simon Eschholz, 61378@novasbe.pt; Niklas Weis, 58464@novasbe.pt; Antonius Greiner, 58544@novasbe.pt; Selina Dilger, 59642@novasbe.pt'
author = 'Simon Eschholz, 61378@novasbe.pt; Niklas Weis, 58464@novasbe.pt; Antonius Greiner, 58544@novasbe.pt; Selina Dilger, 59642@novasbe.pt'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
