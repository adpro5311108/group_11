.. Project Icaras - Group 11 documentation master file, created by
   sphinx-quickstart on Sun Mar 17 22:50:21 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Project Icaras - Group 11's documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
