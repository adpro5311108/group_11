"""
This module contains the AirportData class used for managing and analyzing airport data,
including information about routes, airports, airplanes, and airlines. It facilitates data
acquisition from specified URLs, data extraction from downloaded files, and performing
various analyses such as calculating distances between airports, analyzing flight
distances,
plotting airports and flight routes on maps, displaying aircraft and airport
information, and
more. Additionally, it integrates functionalities for data cleaning, datatype casting, and
utilizing external APIs for enriched information retrieval. The class methods include
capabilities for plotting geographic data using Folium, generating markdown lists of
aircraft
models and airports, and interfacing with the OpenAI API to fetch detailed information
about
aircraft models and airports.

Dependencies:
    - os
    - urllib
    - zipfile
    - pandas (pd)
    - numpy (np)
    - matplotlib.pyplot (plt)
    - folium
    - IPython.core.display (Markdown, display, DisplayHandle)
    - openai.OpenAI
    - math (radians, sin, cos, asin, sqrt)

Features:
    - Data downloading and extraction
    - Dataframe creation and management
    - Distance calculations using the Haversine formula
    - Data visualization on maps
    - Integration with external APIs for data enrichment
"""

import os
import urllib
import zipfile
import typing
from typing import Optional, Union, Tuple
import matplotlib.pyplot as plt
import numpy as np
from openai import OpenAI
import distance_calculator
import pandas as pd
import folium
from IPython.display import display, Markdown, DisplayHandle


class AirportData:
    """
    A class for managing airport data including routes, airports, airplanes, and airlines.

    Attributes:
        filenames (list): A list of filenames for required data files.
        download_dir (str): The directory where data files are downloaded.
        data_url (str): The URL from which data files are downloaded.
        zip_file_name (str): The name of the downloaded zip file.
        download_necessary (bool): A flag indicating whether data download is necessary.
        aircraft_model_prompt_file (str): The filename of the aircraft model prompt
        template.
        airport_prompt_file (str): The filename of the airport prompt template.
        routes_df (pandas.DataFrame): DataFrame containing route data.
        airports_df (pandas.DataFrame): DataFrame containing airport data.
        airplanes_df (pandas.DataFrame): DataFrame containing airplane data.
        airlines_df (pandas.DataFrame): DataFrame containing airline data.
    """
    filenames = ['routes.csv', 'airports.csv', 'airplanes.csv', 'airlines.csv']
    download_dir = 'downloads/'
    data_url = ('https://gitlab.com/adpro1/adpro2024/-/raw/main/Files/flight_data.zip'
                '?inline=false')
    zip_file_name = 'flight_data.zip'
    download_necessary = False
    aircraft_model_prompt_file = 'aircraft_model_prompt_template.txt'
    airport_prompt_file = 'airport_prompt_template.txt'

    def __init__(self):
        """
        Initializes AirportData class by checking, downloading, and importing necessary
        data files.
        """
        # Checking whether files are aleady downloaded
        for filename in self.filenames:
            if not os.path.exists(self.download_dir + filename):
                print(f"Datafile '{self.download_dir + filename}' not found.")
                self.download_necessary = True
            else:
                print(f"File {self.download_dir + filename}' found.")

        # Download if necessary
        if self.download_necessary:
            print('Downloading all files...')
            # Download the file
            urllib.request.urlretrieve(self.data_url,
                                       self.download_dir + self.zip_file_name)

            # Check if the file has been successfully downloaded
            if os.path.exists(self.download_dir + self.zip_file_name):
                # Extract the downloaded zip file
                with zipfile.ZipFile(self.download_dir + self.zip_file_name,
                                     'r') as zip_ref:
                    zip_ref.extractall(self.download_dir)
                print("Download and extraction complete.")
            else:
                print("Failed to download data files.")

        # Reading data into dataframes
        print('Importing Datafiles...')
        self.routes_df = pd.read_csv(self.download_dir + self.filenames[0],
                                     index_col='index')
        self.airports_df = pd.read_csv(self.download_dir + self.filenames[1],
                                       index_col='index')
        self.airplanes_df = pd.read_csv(self.download_dir + self.filenames[2],
                                        index_col='index')
        self.airlines_df = pd.read_csv(self.download_dir + self.filenames[3],
                                       index_col='index')
        print('Import done.')

        # Data cleaning
        # routes_df: replace '\N' with NaN
        self.routes_df["Source airport ID"] = (
            self.routes_df["Source airport ID"].replace('\\N', np.nan)
        )
        self.routes_df["Airline ID"] = (
            self.routes_df["Airline ID"].replace('\\N', np.nan)
        )
        self.routes_df["Destination airport ID"] = (
            self.routes_df["Destination airport ID"].replace('\\N', np.nan)
        )
        self.routes_df = (
            self.routes_df.dropna(subset=['Source airport ID', 'Destination airport ID'])
        )

        # Datatype casting
        # routes_df
        self.routes_df['Airline ID'] = self.routes_df["Airline ID"].astype(float)
        self.routes_df['Source airport ID'] = (
            self.routes_df["Source airport ID"].astype(float)
        )
        self.routes_df['Destination airport ID'] = (
            self.routes_df["Destination airport ID"].astype(float)
        )

        # airports_df
        self.airports_df['Airport ID'] = self.airports_df["Airport ID"].astype(float)

        self.aircraft_names = self.airplanes_df['Name'].unique()
        self.airport_names = self.airports_df['Name'].unique()

    def get_coordinates_and_calculate_distance(
        self,
        row: pd.Series,
        airports_df: pd.DataFrame
    ) -> float:
        """
        Get the latitude and longitude coordinates for source and destination airports
        from the given DataFrame `airports_df`, and calculate the distance between them.

        Parameters:
            self: Object instance of the class containing this method.
            row (pandas.Series): A pandas Series representing a row of flight data,
                                  containing 'Source airport ID' and 'Destination
                                  airport ID'.
            airports_df (pandas.DataFrame): A DataFrame containing airport information
                                            including 'Airport ID', 'Latitude',
                                            and 'Longitude'.

        Returns:
            float: The distance (in kilometers) between the source and destination
            airports.
        """
        try:
            source_id = row['Source airport ID']
            destination_id = row['Destination airport ID']

            source_airport = airports_df.loc[airports_df['Airport ID'] == source_id]
            destination_airport = airports_df.loc[
                airports_df['Airport ID'] == destination_id]

            source_latitude = source_airport['Latitude'].values[0]
            source_longitude = source_airport['Longitude'].values[0]

            destination_latitude = destination_airport['Latitude'].values[0]
            destination_longitude = destination_airport['Longitude'].values[0]

        except IndexError:
            # should only happen if airports cannot be matched over their id, cannot be found
            # print(':(')
            # print(exception)
            pass
        else:
            return distance_calculator.calculate_distance(source_latitude, source_longitude,
                                                      destination_latitude,
                                                      destination_longitude)

    def distance_analysis(self) -> None:
        """
        Analyze and plot the distribution of flight distances.

        This method iterates over the DataFrame containing flight route data,
        calculates the great circle distance between origin and destination airports,
        and plots the distribution of these distances.
        """
        self.routes_df['Distance'] = (
            self.routes_df.apply(lambda row: self \
                                 .get_coordinates_and_calculate_distance(row, self \
                                                                         .airports_df),
                                 axis=1)
        )
        # Plotting the distribution of distances
        plt.figure(figsize=(10, 6))
        plt.hist(self.routes_df['Distance'], bins=100, color='blue', alpha=0.7)
        plt.title('Distribution of Flight Distances')
        plt.xlabel('Distance (km)')
        plt.ylabel('Frequency')
        plt.grid(True)
        plt.show()

    def plot_airports_in_country(self, country: str) -> None:
        """
        Plots the airports in the specified country on a map.

        Args:
            country (str): The name of the country.

        Returns:
            None

        Raises:
            None
        """

        if country in self.airports_df['Country'].values:
            airports_to_plot = self.airports_df.loc[
                self.airports_df['Country'] == country]

            # create a map
            this_map = folium.Map(prefer_canvas=True)

            def plot_dot(dataframe: pd.Series) -> None:
                '''
                input: series that contains a numeric named latitude
                and a numeric named longitude this function creates a 
                CircleMarker and adds it to your this_map
                '''
                folium.CircleMarker(location=[dataframe.Latitude, dataframe.Longitude],
                                    radius=2,
                                    weight=5).add_child(
                    folium.Tooltip(
                        f"ID {int(dataframe['Airport ID'])} - {dataframe.Name}")).add_to(
                    this_map)

            # use df.apply(,axis=1) to "iterate" through every row in your dataframe
            airports_to_plot.apply(plot_dot, axis=1)

            # Set the zoom to the maximum possible
            this_map.fit_bounds(this_map.get_bounds())
            display(this_map)
        else:
            print('Country ' + country + ' cannot be found in the airports dataframe')

    def plot_flights_from_airport(self, airport_code: str, internal: bool = False) -> None:

        """Plots flights from a given airport on a map.
            Parameters:
                airport_code (str): 
                The code of the airport to plot flights from.
            
                internal (bool, optional): 
                If True, only plots flights to airports in the same country as 
                the source airport.
                Defaults to False.

            This method plots flights from a given airport on a map using the 
            Folium library. It retrieves the coordinates of the source and destination 
            airports from the airports dataframe and adds markers and polylines to the 
            map to represent the flights. If the internal parameter is set to True, 
            it only plots flights to airports in the same country as the source airport.

            If the airport code is not found in the routes dataframe, 
            it prints a message indicating that the airport cannot be found.

            Note: This method requires the Folium library to be installed.

            Example usage:
            >>> plot_flights_from_airport('JFK', internal=True)"""

        if airport_code in self.routes_df['Source airport'].values:
            routes_to_plot = self.routes_df.loc[
                self.routes_df['Source airport'] == airport_code].copy()
            if internal:
                airport_country = \
                    self.airports_df.loc[
                        self.airports_df['Airport ID'] == routes_to_plot.iloc[0][
                            'Source airport ID']][
                        'Country'].values[0]

                dest_airports_to_plot = \
                    self.airports_df.loc[self.airports_df['Country'] == airport_country][
                        'IATA']
                routes_to_plot = routes_to_plot[
                    routes_to_plot['Destination airport'].isin(dest_airports_to_plot)]

            def get_coordinates(airport_id: float) -> typing.Tuple[float, float]:
                airport_lat = \
                    self.airports_df.loc[self.airports_df['Airport ID'] == airport_id][
                        'Latitude'].values[0]
                airport_long = \
                    self.airports_df.loc[self.airports_df['Airport ID'] == airport_id][
                        'Longitude'].values[0]
                return airport_lat, airport_long

            routes_to_plot['Source lat'] = routes_to_plot['Source airport ID'].apply(
                lambda x: get_coordinates(x)[0])
            routes_to_plot['Source long'] = routes_to_plot['Source airport ID'].apply(
                lambda x: get_coordinates(x)[1])

            routes_to_plot['Destination lat'] = routes_to_plot[
                'Destination airport ID'].apply(
                lambda x: get_coordinates(x)[0])
            routes_to_plot['Destination long'] = routes_to_plot[
                'Destination airport ID'].apply(
                lambda x: get_coordinates(x)[1])

            # create a map
            this_map = folium.Map(prefer_canvas=True)

            def plot_route(dataframe: pd.Series) -> None:
                folium.CircleMarker(
                    location=[dataframe['Source lat'], dataframe['Source long']],
                    radius=2,
                    weight=5).add_child(
                    folium.Tooltip(
                        f"ID: {int(dataframe['Source airport ID'])} - "
                        f"{dataframe['Source airport']}")).add_to(
                    this_map)
                folium.CircleMarker(location=[dataframe['Destination lat'],
                                              dataframe['Destination long']],
                                    radius=2,
                                    weight=5).add_child(folium.Tooltip(
                    f"ID: {int(dataframe['Destination airport ID'])} - "
                    f"{dataframe['Destination airport']}")).add_to(
                    this_map)
                folium.PolyLine([[dataframe['Source lat'], dataframe['Source long']],
                                 [dataframe['Destination lat'],
                                  dataframe['Destination long']]]).add_to(this_map)

            # use df.apply(,axis=1) to "iterate" through every row in your dataframe
            routes_to_plot.apply(plot_route, axis=1)

            # Set the zoom to the maximum possible
            this_map.fit_bounds(this_map.get_bounds())
            display(this_map)

        else:
            print('Airport ' + airport_code + ' cannot be found in the routes dataframe')

    def plot_flights_from_country(self, country: str, internal: bool = False) -> None:
        """
        Plots flights from a specific country on a map.

        Parameters:
            country (str): The name of the country.
            internal (bool, optional): If True, only plots internal flights within the
            country.
                                      If False, plots all flights from the country.
                                      Default is False.
        """

        if country in self.airports_df['Country'].values:
            src_airports_to_plot = \
                self.airports_df.loc[self.airports_df['Country'] == country][
                    'Airport ID'].copy()
            routes_to_plot = self.routes_df[
                self.routes_df['Source airport ID'].isin(src_airports_to_plot)].copy()

            if internal:
                dest_airports_to_plot = \
                    self.airports_df.loc[self.airports_df['Country'] == country]['IATA']
                routes_to_plot = routes_to_plot[
                    routes_to_plot['Destination airport'].isin(dest_airports_to_plot)]

            def get_coordinates(airport_id: int) -> typing.Tuple[float, float]:
                airport_lat = \
                    self.airports_df.loc[self.airports_df['Airport ID'] == airport_id][
                        'Latitude'].values[0]
                airport_long = \
                    self.airports_df.loc[self.airports_df['Airport ID'] == airport_id][
                        'Longitude'].values[0]
                return airport_lat, airport_long

            routes_to_plot['Source lat'] = routes_to_plot['Source airport ID'].apply(
                lambda x: get_coordinates(x)[0])
            routes_to_plot['Source long'] = routes_to_plot['Source airport ID'].apply(
                lambda x: get_coordinates(x)[1])

            routes_to_plot['Destination lat'] = routes_to_plot[
                'Destination airport ID'].apply(
                lambda x: get_coordinates(x)[0])
            routes_to_plot['Destination long'] = routes_to_plot[
                'Destination airport ID'].apply(
                lambda x: get_coordinates(x)[1])

            # create a map
            this_map = folium.Map(prefer_canvas=True)

            def plot_route(dataframe: pd.Series) -> None:
                folium.CircleMarker(
                    location=[dataframe['Source lat'], dataframe['Source long']],
                    radius=2,
                    weight=5).add_child(
                    folium.Tooltip(
                        f"ID: {int(dataframe['Source airport ID'])} - "
                        f"{dataframe['Source airport']}")).add_to(
                    this_map)
                folium.CircleMarker(location=[dataframe['Destination lat'],
                                              dataframe['Destination long']],
                                    radius=2,
                                    weight=5).add_child(folium.Tooltip(
                    f"ID: {int(dataframe['Destination airport ID'])} - "
                    f"{dataframe['Destination airport']}")).add_to(
                    this_map)
                folium.PolyLine([[dataframe['Source lat'], dataframe['Source long']],
                                 [dataframe['Destination lat'],
                                  dataframe['Destination long']]]).add_to(this_map)

            # use df.apply(,axis=1) to "iterate" through every row in your dataframe
            routes_to_plot.apply(plot_route, axis=1)

            # Set the zoom to the maximum possible
            this_map.fit_bounds(this_map.get_bounds())
            display(this_map)

        else:
            print('Country ' + country + ' cannot be found in the airports dataframe')

    def airplanes(self) -> None:
        """
        Displays a list of aircraft models.

        Returns:
            None
        """
        column_values = self.airplanes_df['Name'].tolist()
        markdown_list = "\n".join(
            [f"{i + 1}. {value}" for i, value in enumerate(column_values)])
        print('List of aircraft models on file:')
        return display(Markdown(markdown_list))

    def airports(self) -> None:
        """
        Displays a list of airports.

        Returns:
            None
        """
        column_values = self.airports_df['Name'].tolist()
        markdown_list = "\n".join(
            [f"{i + 1}. {value}" for i, value in enumerate(column_values)])
        print('List of airports on file:')
        return display(Markdown(markdown_list))

    def aircraft_info(self, aircraft_name: str) -> DisplayHandle:
        """
        Retrieves information about a specific aircraft model.

        Args:
            aircraft_name (str): The name of the aircraft model.

        Returns:
            DisplayHandle: A handle to display the information.

        Raises:
            ValueError: If the specified aircraft model is not recognized.

        """
        # Use the list stored in self.aircraft_names
        try:
            if aircraft_name not in self.aircraft_names:
                raise ValueError(
                    f"{aircraft_name} is not a recognized aircraft model. Please choose "
                    f"a model from the following list:\n")
        except ValueError as e:
            print(e)
            return self.airplanes()

        OpenAI.api_key = os.getenv("OPENAI_API_KEY")
        client = OpenAI()

        def populate_airplane_model_prompt(
                prompt_file: str,
                airplane_model_name: str
        ) -> str:
            with open(prompt_file, 'r', encoding='utf-8') as file:
                prompt = file.read()
            populated_prompt = prompt.replace('[Airplane Model Name]', airplane_model_name)
            return populated_prompt

        completion = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "user", "content": populate_airplane_model_prompt(
                self.aircraft_model_prompt_file, aircraft_name)}
                      ]
        )
        # Extract the message content from the completion
        message_content = completion.choices[0].message.content

        return display(Markdown(message_content))

    def airport_info(self, airport_name: str) -> DisplayHandle:
        """
        Retrieves main information like about a specific airport in a table format.

        Args:
            airport_name (str): The name of the airport. Use the official airport name
            not the IATA code.

        Returns:
            DisplayHandle: A handle to display the information.

        Raises:
            ValueError: If the provided airport name is not recognized.

        """
        # Use the list stored in self.aircraft_names
        try:
            if airport_name not in self.airport_names:
                raise ValueError(
                    f"{airport_name} is not a recognized airport. \n")
        except ValueError as e:
            raise e
            # Too noisy
            # return self.airports()

        OpenAI.api_key = os.getenv("OPENAI_API_KEY")
        client = OpenAI()

        def populate_airport_prompt(prompt_file: str, airplane_model_name: str) -> str:
            with open(prompt_file, 'r', encoding='utf-8') as file:
                prompt = file.read()
            populated_prompt = prompt.replace('[Airport Name]', airplane_model_name)
            return populated_prompt

        completion = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "user",
                       "content": populate_airport_prompt(self.airport_prompt_file,
                                                          airport_name)}
                      ]
        )
        # Extract the message content from the completion
        message_content = completion.choices[0].message.content

        return display(Markdown(message_content))

    def models_per_route_country(
            self,
            country: Optional[str] = None,
            n: int = 10
        ) -> None:
        """
        Calculate and plot the N most common airplane models per route for a given
        country.

        Parameters:
        - country (str): The country to filter the routes by. If not provided,
        all routes will be considered.
        - N (int): The number of top airplane models to display. Default is 10.
        Calculate and plot the N most common airplane models per route for a given
        country.

        Returns: Plot of the N most common airplane models per route for a given country.
        None
        """

        # Merge dataframes
        routes_airports = self.routes_df.merge(self.airports_df,
                                               left_on='Source airport ID',
                                               right_on='Airport ID')
        routes_airports_planes = routes_airports.merge(self.airplanes_df, how="left",
                                                       left_on='Equipment',
                                                       right_on='IATA code')

        if country is not None:
            if isinstance(country, list):
                routes_country = routes_airports_planes[routes_airports_planes[
                                                        'Country'].isin(country)]
            else:
                routes_country = routes_airports_planes[
                    routes_airports_planes['Country'] == country]
        else:
            routes_country = routes_airports_planes

        # group by airplane model and count number of routes
        airplane_model_counts = routes_country.groupby('Name_y').size().reset_index(
            name='Count')

        # Sort by count in descending order
        airplane_model_counts = airplane_model_counts.sort_values('Count',
                                                                  ascending=False)

        # Select the top N airplane models
        top_airplane_models = airplane_model_counts.head(n)

        if country is None:
            country = "all countries"
        elif isinstance(country, list):
            country = ", ".join(country)

        # Plot the N most used airplane models
        top_airplane_models.plot(x='Name_y', y='Count', kind='bar',
                                 xlabel='Airplane Model', ylabel='Number of Routes',
                                 title="Most common models per route for " + country)

    def plot_flights_per_country(
            self,
            country: str,
            internal: bool = False,
            cutoff: float = 1000.00
    ) -> folium.Map:
        '''
        Displays flight routes originating from a specified country on a map.
        Differentiates between domestic
        (within the same country) and international flights. Shows potential CO2
        emission reductions by replacing
        short-distance flights with rail transport based on flight distances and
        emissions per kilometer.

        Parameters:
        - country (str): Country name where flights originate. Case-sensitive.
        - internal (bool, optional): True for only domestic flights, False for all
        flights. Default is False.
        - cutoff (float, optional): Distance in kilometers to define short-haul
        flights. Default is 1000 km.

        Returns:
         folium.Map: A map with flight routes. Short-haul flights are marked in blue,
         long-haul in orange.
            Includes a legend and annotations like the number of short-haul flights and
            their total distance.


          Raises:
        - TypeError: Incorrect type for any parameter.
        - ValueError: Country not found in the dataset.

        Example Usage:
            """
            # Assuming an instance of the class has been created as "instance"
            map = instance.plot_flights_per_country_with_cutoff("Portugal",
                                                                internal=True,
                                                                cutoff=800.00)
            map.save("portugal_domestic_flights.html")
            """

        Note:
        Requires "self.routes" and "self.airports" for flight and airport data.
        Accuracy depends on these being current and formatted correctly.

        '''

        # Type checking
        if not isinstance(country, str):
            raise TypeError(
                f"The 'country' argument must be of type str, got type"
                f"{type(country).__name__}."
            )
        if not isinstance(internal, bool):
            raise TypeError(
                f"The 'internal' argument must be of type bool, got type"
                f"{type(internal).__name__}."
            )
        if not isinstance(cutoff, float):
            raise TypeError(
                f"The 'cutoff' argument must be of type float, got type"
                f"{type(cutoff).__name__}."
            )

        # Merging with airport information to get coordinates
        merged_df = self.routes_df.merge(
            self.airports_df[["Airport ID", "Country", "Latitude", "Longitude"]],
            left_on="Source airport ID",
            right_on="Airport ID",
            how="left",
        )
        merged_df.rename(
            columns={
                "Country": "origin_country",
                "Latitude": "Source Latitude",
                "Longitude": "Source Longitude",
            },
            inplace=True,
        )
        merged_df = merged_df.merge(
            self.airports_df[["Airport ID", "Country", "Latitude", "Longitude"]],
            left_on="Destination airport ID",
            right_on="Airport ID",
            how="left",
        )
        merged_df.rename(
            columns={
                "Latitude": "Destination Latitude",
                "Longitude": "Destination Longitude",
                "Country": "destination_country",
            },
            inplace=True,
        )

        # Define filtered_df

        if internal:
            # Filtering for domestic flights within the specified country
            filtered_df = merged_df[
                (merged_df["origin_country"] == country) &
                (merged_df["destination_country"] == country)
                ]
        else:
            # Filtering for all flights originating from the specified country
            filtered_df = merged_df[merged_df["origin_country"] == country]

        # Check if any routes match the criteria
        if filtered_df.empty:
            print(
                f"No flights found for country '{country}' with the specified "
                f"parameters.")
            return None  # or handle this case as you see fit


        # Filter out routes by creating  unique identifier per route (regardless
        # direction)
        filtered_df = filtered_df.copy()
        filtered_df.loc[:, "Route ID"] = filtered_df.apply(
            lambda x: "-".join(
                sorted([str(x["Source airport ID"]), str(x["Destination airport ID"])]),
            ),
            axis=1,
        )

        unique_routes_df = filtered_df.drop_duplicates(subset="Route ID").drop(
            "Route ID", axis=1,
        )

        # Calculate the distances and categorize flights
        unique_routes_df["Distance"] = unique_routes_df.apply(
            lambda row: distance_calculator.calculate_distance(
                row["Source Latitude"], row["Source Longitude"],
                row["Destination Latitude"], row["Destination Longitude"]
            ),
            axis=1
        )
        unique_routes_df["Short Haul"] = unique_routes_df["Distance"] < cutoff

        # Calculate the number of short-haul flights and the total distance
        number_short_haul = unique_routes_df["Short Haul"].sum()
        total_distance_short_haul = round(
            unique_routes_df[unique_routes_df["Short Haul"] == True]["Distance"].sum())

        # Calculate emissions for short-haul flights and trains, and the reduction
        co2_emission_planes = 151 / 1000000  # converting grams to tonnes for
        # short-haul flights
        co2_emission_trains = 35 / 1000000  # converting grams to tonnes for rail services
        emission_short_haul_flights = co2_emission_planes * total_distance_short_haul
        emission_trains = co2_emission_trains * total_distance_short_haul
        emission_reduction = round(emission_short_haul_flights - emission_trains, 2)

        # Initialize the map
        fmap = folium.Map(location=[20, 0], zoom_start=2)

        # Add the legend with actual values using f-string for formatting
        legend_html = f"""
        <div style="position: fixed; 
            bottom: 50px; left: 50px; width: 220px; height: 160px; 
            border:2px solid grey; z-index:9999; font-size:14px;
            background-color: rgba(255, 255, 255, 0.8);
        ">&nbsp; <strong>Flight Types</strong> <br>
            &nbsp; Blue : Short-Distance<br>
            &nbsp; Orange : Long-Distance<br>
            &nbsp; <strong>Short-Distance Flights:</strong> {number_short_haul}<br>
            &nbsp; <strong>Total Short-Distance:</strong> {total_distance_short_haul} 
            km<br>
            &nbsp; <strong>CO2 Savings:</strong> {emission_reduction} tonnes<br>
        </div>
        """
        fmap.get_root().html.add_child(folium.Element(legend_html))

        # Plot routes
        for _, row in unique_routes_df.iterrows():
            color = "blue" if row["Short Haul"] else "orange"
            origin = [row["Source Latitude"], row["Source Longitude"]]
            destination = [row["Destination Latitude"], row["Destination Longitude"]]
            folium.PolyLine([origin, destination], color=color, weight=2.5,
                            opacity=1).add_to(fmap)

        return fmap
