"""
Calculate the great circle distance between two points
on the Earth using the Haversine formula.
"""

from math import radians, sin, cos, asin, sqrt

def calculate_distance(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    Parameters:
    - lat1 (float): Latitude of the first point in decimal degrees.
    - lon1 (float): Longitude of the first point in decimal degrees.
    - lat2 (float): Latitude of the second point in decimal degrees.
    - lon2 (float): Longitude of the second point in decimal degrees.

    Returns:
    - distance (float): The distance between the two points in kilometers.

    The Haversine formula calculates the shortest distance over the earth's surface
    between two points, which are specified in decimal degrees of latitude and
    longitude.
    It assumes the Earth as a perfect sphere, ignoring any irregularities.
    """
    # Convert decimal degrees to radians
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of Earth in kilometers
    return c * r
