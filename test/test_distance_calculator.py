"""
Module: test_distance_calculator

A module to perform unit testing of the Distance Calculator module.

This module contains a test suite for the Distance Calculator module,
which calculates distances between geographical locations.

Classes:
-------
TestDistanceCalculator(unittest.TestCase):
    A class to perform unit testing of the Distance Calculator module.

    Methods:
    -------
    setUp(self):
        Set up method to initialize test fixtures.

    test_distance_different_continents(self):
        Test case to calculate the distance between locations on different continents.

    test_distance_same_continent(self):
        Test case to calculate the distance between locations on the same continent.

    test_distance_same_country(self):
        Test case to calculate the distance between locations in the same country.
"""

import unittest
import sys
import os

parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Add the parent directory to the sys.path
sys.path.insert(0, parent_dir)

from distance_calculator import calculate_distance


class TestDistanceCalculator(unittest.TestCase):
    """Class for Unit Testing of Distance Calculator Docstring"""

    def setUp(self):
        pass

    def test_distance_different_continents(self):
        """Testing JFK - LHR Distance (different continent)"""

        # JFK Airport (New York, USA) to LHR Airport (London, UK)
        distance = calculate_distance(40.6413111, -73.7781391,
                                                          51.4700223,
                                                          -0.4542955)
        self.assertAlmostEqual(distance, 5547, delta=10)

    def test_distance_same_continent(self):
        """Testing LAX - JFK Distance (same continent)"""

        # LAX (Los Angeles, USA) to JFK (New York, USA)
        distance = calculate_distance(33.9415889, -118.40853,
                                                          40.6413111,
                                                          -73.7781391)
        self.assertAlmostEqual(distance, 3983, delta=10)

    def test_distance_same_country(self):
        """Testing SFO - LAX Distance (same country)"""

        # SFO (San Francisco, USA) to LAX (Los Angeles, USA)
        distance = calculate_distance(37.774929, -122.419416,
                                                          33.9415889, -118.40853)
        self.assertAlmostEqual(distance, 559, delta=10)


if __name__ == '__main__':
    unittest.main()
