# Adpro Group 11 - Welcome to project Icaras
Group Members:
- Simon Eschholz, [61378\@novasbe.pt](mailto:61378@novasbe.pt)
- Niklas Weis, [58464\@novasbe.pt](mailto:58464@novasbe.pt)
- Antonius Greiner, [58544\@novasbe.pt](mailto:58544@novasbe.pt)
- Selina Dilger, [59642\@novasbe.pt](mailto:59642@novasbe.pt)

## Introduction
Our project, Icaras, analyzes commercial airflight data to identify potential CO2 emissions reductions by replacing short-haul flights with rail transport. We utilize datasets from the [International Air Transport Association (IATA)](https://www.iata.org/), available in our `downloads/` directory and from the [IATA data repository](https://gitlab.com/adpro1/adpro2024/-/raw/main/Files/flight_data.zip?inline=false).

## Setup
To get started with the Icaras project:
1. Ensure Python is installed on your machine.
2. Clone the repository to your local system.
3. Create and activate the Conda environment with `conda env create -f conda-env.yml`.
4. Install the required dependencies using `pip install -r requirements.txt`.

## Project Structure
- `Showcase.ipynb`: Demonstrative Jupyter Notebook for our analysis.
- `airport_data.py`: Contains the `AirportData` class for data management and analysis.
- `distance_calculator.py`: Utility for calculating distances between airports.
- `downloads/`: Directory where data files are downloaded.
- `test/`: Unit tests for the application.
- `requirements.txt`: Lists the Python package dependencies.
- `LICENSE`: The license file for the project.

## Methods Overview
The `AirportData` class includes methods for:
- Calculating distances between airports.
- Analyzing distribution of flight distances.
- Plotting airports and flight routes on maps.
- Visualizing detailed flight data and differentiating between short and long-haul flights.
- Estimating potential CO2 emissions savings by substituting short-haul flights with rail services.
- Analyzing aircraft utilization across various routes and countries.

## Usage
Explore our analysis with `Showcase.ipynb`:
1. Ensure the Conda environment is active.
2. Launch Jupyter Notebook or Lab.
3. Open and execute `Showcase.ipynb` to view our analysis and visualizations.

## Showcase Story
The `Showcase.ipynb` notebook guides you through our analysis of flight data from Portugal and Namibia. We showcase how our `AirportData` methods can be applied to examine flight patterns and assess the CO2 emissions impact of replacing short-haul flights with railway services for the capital airports of each country.

## Documentation
You can find our project in `/docs/` directory. Run `make html` from this directory to generate the latest version of the documentation using Sphinx.

## Final Remarks
Join us in exploring the data and uncovering insights that could lead to more sustainable aviation practices.
